# NAPREDNE BAZE PODATAKA #

Za svaki projekat kreirana je posebna grana u kojoj se nalazi implementacija.

### Grana 1: BasketScore-Redis 

* [Link to branch](https://bitbucket.org/longfield16545/napredne-baze-podataka/src/BasketScore-Redis/)

### Grana 2: ITForMe-Cassandra 

* [Link to branch](https://bitbucket.org/longfield16545/napredne-baze-podataka/src/ITForMe-Cassandra/)

### Grana 3: JATDentist-MongoDB

* [Link to branch](https://bitbucket.org/longfield16545/napredne-baze-podataka/src/JATDentist-MongoDB/)

### Set up ###

Set up svakog projekta bice napisan u njegovom README.md file-u!